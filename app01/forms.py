from django import forms

from app01.models import Users

class Usersforms (forms.ModelForm):

    class Meta:
        model = Users

        fields = [
        
            'usr_name',
            'email',
            'password',

        ]

        labels = {

            'usr_name': 'nombre de usuario',
            'email': 'correo',
            'password': 'clave'

        }

        widgets = {

            'usr_name': forms.TextInput(attrs = {'class':'form-control'}),
            'email', forms.TextInput(attrs = {'class':'form-control'}),
            'password', forms.TextInput(attrs = {'class':'form-control'}),

        }