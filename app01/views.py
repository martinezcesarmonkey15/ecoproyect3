from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse
from django.http import Http404
from django.template import loader
from django.forms import forms
from django.views.generic import TemplateView

# Create your views here.

# def index(request):
#    return HttpResponse("Hello, world. You're at the polls index.")


class index(TemplateView):
    template_name = "registration/login.html"


def home(request):
    return render(request, 'app01/home.html')


def instructivo(request):
    return render(request, 'app01/instructivo.html')


def reciclaje(request):
    return render(request, 'app01/reciclaje.html')


def contacto(request, identificador):
    print(identificador)
    return render(request, 'app01/contacto.html')


def empresas(request):
    return render(request, 'app01/empresas.html')


def ecodoku(request):
    return render(request, 'app01/ecodoku.html')


def registrarse(request):
    return render(request, 'app01/index.html')


def bodega_usr(request):
    return render(request, 'app01/bodega_usr.html')


def mis_ventas(request):
    return render(request, 'app01/mis_ventas.html')


def show_ecocompras(request):
    return render(request, 'app01/show_ecocompras.html')


def show_ventas_usr(request):
    return render(request, 'app01/show_ventas_usr.html')
