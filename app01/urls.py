from django.urls import path
from django.contrib.auth.decorators import login_required


from . import views

app_name = 'app01'

urlpatterns = [
    path('', login_required(views.index.as_view()), name='index'),
    path('instructivo', login_required(views.instructivo), name='instructivo'),
    path('home', login_required(views.home), name='home'),
    path('reciclaje', login_required(views.reciclaje), name='reciclaje'),
    path('contacto/<int:identificador>', login_required(views.contacto), name='contacto'),
    path('empresas', login_required(views.empresas), name='empresas'),
    path('bodega_usr', login_required(views.bodega_usr), name='bodega_usr'),
    path('mis_ventas', login_required(views.mis_ventas), name='mis_ventas'),
    path('show_ecocompras', login_required(views.show_ecocompras), name='show_ecocompras'),
    path('show_ventas_usr', login_required(views.show_ventas_usr), name='show_ventas_usr'),
    # path('registrar', views.registrar)
]